﻿function VideoData(name) {
    this.videoName = "Quizzer25";
    this.startImageName = "Quizzer"
    this.snapShotTimes = [0, 39, 43, 115, 119, 190, 214, 217, 227, 237, 239, 245, 248];

    this.activityName = "Mathappinterface2";
    this.activityType = "Plain"
    this.difficulty = "Beginner";

    this.thumbName = "OrigamiVideoPiece";
    this.count = 9;

    if (name == "butterfly") {
        this.videoName = "Butterfly";
        this.startImageName = "Butterfly"
        this.snapShotTimes = [4, 31, 49, 50, 52, 68, 104, 119, 162, 165, 174, 180, 199];

        this.activityName = "Mathappinterface1";
        this.difficulty = "Beginner";
        this.activityType = "Plain"

        this.thumbName = "butterfly" + this.thumbName;
        this.count = 13;
    }
    else if (name == "bird") {
        this.videoName = "Bird";
        this.startImageName = "Bird"

        this.snapShotTimes = [0, 22, 46, 70, 83, 93, 116, 117, 125];

        this.activityName = "Mathappinterface2";
        this.difficulty = "Beginner";
        this.activityType = "Plain"

        this.thumbName = "blue" + this.thumbName;
        this.count = 9;
    }

    else if (name == "panda") {
        this.videoName = "Panda";
        this.startImageName = "Panda"
        this.snapShotTimes = [0, 54, 93, 124, 142, 145, 172, 180, 184];

        this.activityName = "Mathappinterface3";
        this.difficulty = "Beginner";
        this.activityType = "Plain"

        this.thumbName = "purple" + this.thumbName;
        this.count = 9;

    }
    else if (name == "loin") {
        this.videoName = "Lion";
        this.startImageName = "Lion"
        this.snapShotTimes = [0, 22, 60, 96, 135, 158, 178, 188, 205, 215, 228, 233, 240, 245];

        this.activityName = "Mathappinterface1";
        this.difficulty = "Intermediate";
        this.activityType = "Plain"

        this.thumbName = "orange" + this.thumbName;
        this.count = 14;

    }
    else if (name == "bomb") {
        this.videoName = "Bomb";
        this.startImageName = "Bomb"
        this.snapShotTimes = [0, 19, 34, 46, 72, 74, 106, 122, 138, 161, 178, 244, 251];

        this.activityName = "MathWaterbombVolume";
        this.difficulty = "Low Intermediate";
        this.activityType = "Cube"

        this.thumbName = "red" + this.thumbName;
        this.count = 13;

    }
    else if (name == "flower") {
        this.videoName = "FlowerBox";
        this.startImageName = "StarBox"
        this.snapShotTimes = [0, 60, 104, 115, 117, 127, 135, 165, 197, 198, 218, 220, 252, 255, 286, 313, 333, 337, 354, 418, 445];

        this.activityName = "MathStarboxVolume";
        this.difficulty = "Intermediate";
        this.activityType = "Pyramid"

        this.thumbName = "pink" + this.thumbName;
        this.count = 21;

    }
    else if (name == "chair") {
        this.videoName = "Chair";
        this.startImageName = "Chair"
        this.snapShotTimes = [0, 92, 163, 204, 255, 293, 357, 374, 435, 535, 541, 559, 566, 570];

        this.activityName = "MathChairRatios";
        this.difficulty = "Intermediate";
        this.activityType = "Plain"

        this.thumbName = "green" + this.thumbName;
        this.count = 14;

    }
    else if (name == "chomper") {
        this.videoName = "Chomper";
        this.startImageName = "Chomper"
        this.snapShotTimes = [0, 22, 62, 102, 111, 128, 134, 147, 160, 188, 210, 217, 218];

        this.activityName = "Mathappinterface3";
        this.difficulty = "Beginner";
        this.activityType = "Plain"

        this.thumbName = "chomper" + this.thumbName;
        this.count = 13;

    }
    else if (name == "squaretwist") {
        this.videoName = "SquareTwist";
        this.startImageName = "Twist"
        this.snapShotTimes = [0, 17, 40, 55, 73, 90, 105, 119, 143, 176, 212, 217, 223, 228, 230, 236];

        this.activityName = "MathSquareTwistArea";
        this.difficulty = "Low Intermediate";
        this.activityType = "Square"

        this.thumbName = "squaretwist" + this.thumbName;
        this.count = 16;

    }
    else if (name == "flasher") {
        this.videoName = "Flasher";
        this.startImageName = "Flasher"
        this.snapShotTimes = [0, 32, 48, 83, 98, 123, 152, 177, 188, 210, 242, 266, 278, 300, 309, 332, 354, 378, 413, 415, 416, 420];

        this.activityName = "MathFlasherArea";
        this.difficulty = "Complex";
        this.activityType = "Square"

        this.thumbName = "flasher" + this.thumbName;
        this.count = 22;

    }
    else if (name == "quizzer") {
        this.videoName = "Quizzer";
        this.startImageName = "Quizzer"
        this.snapShotTimes = [0, 39, 43, 115, 119, 190, 214, 217, 227, 237, 239, 245, 248];

        this.activityName = "Mathappinterface2";
        this.difficulty = "Beginner";
        this.activityType = "Plain"

        this.thumbName = "quizzer" + this.thumbName;
        this.count = 13;
    }
    this.videoName = this.videoName + "25.mp4";
    this.startImageName = this.startImageName + "400.png";
    this.activityName = this.activityName + ".png";
}