﻿var currentActivityName = "";
var lastSimpleValid = "1";
var lastPyr1Valid = "3";
var lastPyr2Valid = "4";
var lastPyr3Valid = "5";

function init_ActivityPage(name) {
    currentActivityName = name;
    var activityImage = document.getElementById("actImageTag");
    var currentVideoData = new VideoData(name);
    showRelevantActivityElements(name);
    activityImage.src = "images/" + currentVideoData.activityName;
    activityAnswerChanged();
}

function dispose_ActivityPage() {
    currentActivityName = "";
    simpleInput.value = "1";
    pyramidInput1.value = "3";
    pyramidInput2.value = "4";
    pyramidInput3.value = "5";
    lastSimpleValid = "1";
    lastPyr1Valid = "3";
    lastPyr2Valid = "4";
    lastPyr3Valid = "5";
}

function showRelevantActivityElements(name)
{
    var simpleAnwser = document.getElementById("simpleAnwser");
    var fancyAnwser = document.getElementById("fancyAnwser");
    var simpleInput = document.getElementById("simpleInput");
    var pyramidInput1 = document.getElementById("pyramidInput1");
    var pyramidInput2 = document.getElementById("pyramidInput2");
    var pyramidInput3 = document.getElementById("pyramidInput3");

    var hiddenClass = "activityFont hide";
    var visibleClass = "activityFont";

    simpleAnwser.className = hiddenClass;
    fancyAnwser.className = hiddenClass;
    simpleInput.className = hiddenClass;
    pyramidInput1.className = hiddenClass;
    pyramidInput2.className = hiddenClass;
    pyramidInput3.className = hiddenClass;

    if(name == "bomb" || name == "squaretwist" || name == "flasher")
    {
        simpleAnwser.className = visibleClass;
        fancyAnwser.className = visibleClass;
        simpleInput.className = visibleClass;
    }
    if(name == "flower")
    {
        simpleAnwser.className = visibleClass;
        fancyAnwser.className = visibleClass;
        pyramidInput1.className = visibleClass;
        pyramidInput2.className = visibleClass;
        pyramidInput3.className = visibleClass;
    }
}
function activityAnswerChanged()
{
    if (currentActivityName != "") {
        var anwser = 0;
        var bottomAnwser = "";
        if (simpleInput.value < 0) {
            simpleInput.value = 0;
        }
        if (pyramidInput1.value < 0) {
            pyramidInput1.value = 0;
        }
        if (pyramidInput2.value < 0) {
            pyramidInput2.value = 0;
        }
        if (pyramidInput3.value < 0) {
            pyramidInput3.value = 0;
        }
        if ( currentActivityName == "squaretwist" || currentActivityName == "flasher") {
            var inputvalue = simpleInput.value;
            anwser = inputvalue * inputvalue;
            bottomAnwser = "" + inputvalue + "&sup2 = " + anwser;
        }
        if (currentActivityName == "bomb") {
            var inputvalue = simpleInput.value;
            anwser = inputvalue * inputvalue * inputvalue;
            bottomAnwser = "" + inputvalue + "&sup3 = " + anwser;

        }
        if (currentActivityName == "flower") {
            var inputvalue1 = pyramidInput1.value;
            var inputvalue2 = pyramidInput2.value;
            var inputvalue3 = pyramidInput3.value;
            anwser = inputvalue1 * inputvalue2 * inputvalue3 /3;
            bottomAnwser = "" + inputvalue1 + " x " + inputvalue2 + " x " + inputvalue3 + " / 3 = " + anwser;

        }

        lastSimpleValid = "1";
        lastPyr1Valid = "3";
        lastPyr2Valid = "4";
        lastPyr3Valid = "5";

        simpleAnwser.innerHTML = anwser;
        fancyAnwser.innerHTML = bottomAnwser;
    }
}

function GetChar(event , sender) {

    if (currentActivityName == "") {
        event.preventDefault();
        return;
    }

    var keyCode = ('which' in event) ? event.which : event.keyCode;
    if(keyCode >= 48 && keyCode <= 57 )//0-9
    {
        if (sender.value.length > 7) {
            event.preventDefault();
            return;
        }

      //  sender.value += String.fromCharCode(keyCode);
    }
    else if(keyCode ==8)
    {
       // if (sender.value.length > 0)
       //     sender.value = sender.value.substr(0, sender.value.length - 1);
    }
    else if (keyCode == 13) {
        activityAnswerChanged();
        // if (sender.value.length > 0)
        //     sender.value = sender.value.substr(0, sender.value.length - 1);
    }
    else if(keyCode == 190) //.
    {
        if (sender.value.indexOf(".") != -1) {
            event.preventDefault();

            return;
        }
     //   sender.value += ".";
    }
    else
    {
        event.preventDefault();
    }
}
function IsNumeric(input) {
    return !isNaN(parseFloat(input));
}