﻿var myOrigamiData = new OrigamiData();

function init_MainPage() {
    populateImageList();
}
function dispose_MainPage() {
    GalleryMain.clearItems();
}
function populateImageList() {
    GalleryMain.clearItems();

    for (var i = 0; i < myOrigamiData.choices.length; i++) {
        var name = myOrigamiData.choices[i];
        var currentVideoData = new VideoData(name);

        var $new = $('<li><a href="#"><img src="images/' + currentVideoData.startImageName + '" data-large="images/thumbs/blank.png" alt="' + name + '" data-description="moo" /></a></li>');
        GalleryMain.addItems($new);
    }
    changeMainSpotVisually(GalleryMain.getCurrent());
}
function selectOrigami(frameNumber) {
    origamiSelected = myOrigamiData.choices[frameNumber];
    showView("VideoPage");
}
function changeMainSpotVisually(frameNumber) {
    GalleryMain.selectCurrentItem(frameNumber);
}
function changeDifficultyTxt(indexNumber)
{
    var currentVideoData = new VideoData(myOrigamiData.choices[indexNumber]);

    var txtDifficulty = document.getElementById("txtDifficulty");
    txtDifficulty.innerHTML = "Difficulty: " + currentVideoData.difficulty;
}