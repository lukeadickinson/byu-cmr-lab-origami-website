﻿var movieState = "stopped";
var currentVideoFrame = 0;
var currentVideoData = null;
var videoTimer = null;
function init_VideoPage(name)
{
    var nextButton = document.getElementById("showPage3Btn");
    if (name == "quizzer") {
        showPage3Btn.innerHTML = "Return To Menu";
    }
    else
    {
        showPage3Btn.innerHTML = "Continue To Learning Activity";
    }
    populateImageListVideo(name);

}
function dispose_VideoPage() {
    unloadVideo();
}

function populateImageListVideo(name) {
    Gallery.clearItems();
    currentVideoData = new VideoData(name);

    for (var i = 1; i <= currentVideoData.count; i++) {
        var $new = $('<li><a href="#"><img src="images/thumbs/' + currentVideoData.thumbName + i + '.png" data-large="images/thumbs/blank.png" alt="' + name + '" data-description="moo" /></a></li>');
        Gallery.addItems($new);
    }
    loadVideo(currentVideoData.videoName);

    //changeVideoSpot(37);
    toggleVideo();

}

function loadVideo(videoName) {
    var myVideo = document.getElementById("videoElem");

    myVideo.onended = function (e) {
        var playhead = document.getElementById("playhead");
        playhead.style.visibility = "visible";
    };

    videoTimer = setInterval(function () { myTimer() }, 1000);

    myVideo.src = "videos/" + videoName;
    myVideo.load();
    changeVideoSpot(0);

}
function unloadVideo() {
    var myVideo = document.getElementById("videoElem");
    if (videoTimer != null)
        clearInterval(videoTimer);
    myVideo.src = "";
}
function myTimer() {

    if (movieState == "playing")
    {
        var myVideo = document.getElementById("videoElem");
        var currentVideoTime = myVideo.currentTime;
        //at end/ or last frame
        if(currentVideoFrame >= currentVideoData.snapShotTimes.length)
        {
            return;
        }
        //not at last frame
        var nextThreshold = currentVideoData.snapShotTimes[currentVideoFrame + 1];
        if (currentVideoTime >= nextThreshold)
            changeVideoSpotVisually(currentVideoFrame + 1);
    }

}
function calculateCurrentFrame()
{
    var myVideo = document.getElementById("videoElem");
    var currentVideoTime = myVideo.currentTime;

    if (currentVideoTime == 0 && currentVideoData.snapShotTimes[0] != 0)
    {
        changeVideoSpot(currentVideoData.snapShotTimes[0]);
        return 0;
    }

    for (var i = 0; i < currentVideoData.snapShotTimes.length -1; i++) {
        if (currentVideoData.snapShotTimes[i] <= currentVideoTime &&
            currentVideoData.snapShotTimes[i + 1] > currentVideoTime)
            return i;
    }
    return currentVideoData.snapShotTimes.length - 1
}


function changeVideoSpotVisually(frameNumber)
{
    var myVideo = document.getElementById("videoElem");
    Gallery.selectCurrentItem(frameNumber);
    currentVideoFrame = frameNumber;

}
function changeVideoSpot(frameNumber)
{

    var myVideo = document.getElementById("videoElem");
    var seconds = currentVideoData.snapShotTimes[frameNumber];
    if (myVideo.readyState != 0) {
            myVideo.currentTime = seconds;

        changeVideoSpotVisually(frameNumber);
    }
}
function toggleVideo() {
    var myVideo = document.getElementById("videoElem");
    var playhead = document.getElementById("playhead");
    if (myVideo.paused) {
        playhead.style.visibility = "hidden";

        movieState = "playing";
        myVideo.play();
        changeVideoSpotVisually(calculateCurrentFrame());
    }
    else {
        playhead.style.visibility = "visible";

        movieState = "stopped";
        myVideo.pause();
    }
}


