﻿var pageState = "MainPage";
var origamiSelected = "bird";

function showView(viewName) {
    $('.view').hide();
    pageState = viewName;
    dispose_MainPage();
    dispose_VideoPage();
    dispose_ActivityPage();

    if (viewName == "MainPage") {
        init_MainPage();
        //origamiSelected = "bird";
    }
    if (viewName == "VideoPage") {

       init_VideoPage(origamiSelected);
    }
    if (viewName == "ActivityPage") {
        if (origamiSelected == "quizzer") {
            init_MainPage();
            viewName = "MainPage";
        }
        else {
            init_ActivityPage(origamiSelected);
        }
    }
    $('#' + viewName).show();
}

$(document).ready(function (e) {
    

    $('[data-launch-view]').click(function (e) {
        e.preventDefault();
        var viewName = $(this).attr('data-launch-view');
        showView(viewName);
    });
    showView("MainPage");
});